package com.example.demoapi.controller;

import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import com.example.demoapi.entities.*;

@RestController
public class ProductController {

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts() {
        var data = new ArrayList<Product>();
        data.add(new Product("Laptop", 20000.0));
        data.add(new Product("Display", 10000.0));
        data.add(new Product("Keyboard", 600.0));

        return ResponseEntity.ok().body(data);
    }


}
